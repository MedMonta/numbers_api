# numbers_api


Exercice d'évaluation recrutement R&D ip-label


### Installation


```sh
$ cd numbers_api
$ npm install 
$ nodemon
```

### Routes

Le serveur fonctionne sur : http://localhost:8080

| Route |  Méthode |  Description |
| ------ | ------ | ----|
| /numbers | GET |Renvoyer la liste des nombres
| /numbers | POST |Ajouter un nouveau numéro
| /numbers | DELETE | Supprimer un numéro 
| /numbersAverage | GET | Calculer la moyenne de la liste 
| /numbersMedian | GET |Calculer la médiane de la liste 
| /nuumberAddition | POST | Ajouter une valeur à tous les elements de la liste 
| /numberSoustraction | POST |Soustraire une valeur de tous les elements de la liste
| /numberMultiplication | POST |Multiplier tous les elements de la liste par une valeur
| /numberDivision' | POST |Diviser tous les elements de la liste par une valeur
