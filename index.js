const express = require("express");
const bodyparser = require("body-parser");
const fs = require('fs');
const server = express();
const hostname = 'localhost'; 
const port = 8080; 
const router = express.Router(); 
let arrayOfNumbers = null;
const dataPath= './data/numbers.json'


function getNumbersFromJson () {
  const data = JSON.parse(fs.readFileSync(dataPath));
  arrayOfNumbers = data.numbers;
}

function saveList(arrayOfNumbers) {
  let data = {
    numbers: arrayOfNumbers
  };
  data = JSON.stringify(data);
  fs.writeFileSync(dataPath, data);      
}

getNumbersFromJson();

server.use(bodyparser.json());

router.route('/numbers')
  .get(function(req,res){ 
      res.json({
        success : true,
        numbers : arrayOfNumbers
      });
  })
  .post(function(req,res){
    const numbers = req.body.numbers;
    arrayOfNumbers = arrayOfNumbers.concat(numbers);
    saveList(arrayOfNumbers);
    res.json({
      result: 'success',
      message: 'Nombre ajouté',
      tb: arrayOfNumbers
    });        
  })
  .delete(function(req,res){
    const numbersTodelete = req.body.numbers ;  
    for (let index = 0; index < arrayOfNumbers.length; index++) {
      const currentNb = arrayOfNumbers[index];
      if (numbersTodelete.includes(currentNb)) {
        const index = arrayOfNumbers.indexOf(currentNb);
        arrayOfNumbers.splice(index, 1);     
      }
		}
    saveList(arrayOfNumbers);            
    res.json({
      result: 'success',
      message: 'Numbers deleted'
    })
  }); 

router.route('/numbersAverage')
  .get(function (req, res) {
    let sum = 0;
    const nbOfElements = arrayOfNumbers.length;
    for (let index = 0; index < arrayOfNumbers.length; index++) {
      const currentNb = arrayOfNumbers[index];
      sum += currentNb;
    }
    const average = sum / nbOfElements;
    res.json({
      success: true,
      average: average
    });  
  }) 

router.route('/numbersMedian')
  .get(function (req, res) {
    const median = arrayOfNumbers => {
      const mid = Math.floor(arrayOfNumbers.length / 2);
      const nums = [...arrayOfNumbers].sort((a, b) => a - b);
      return arrayOfNumbers.length % 2 !== 0 ? nums[mid] : (nums[mid - 1] + nums[mid]) / 2; 
    };
    numbersMedian =median(arrayOfNumbers);    
    res.json({
      success: true,
      median: numbersMedian
    });
  }) 

router.route('/numberAddition')
  .post(function (req, res) {
    const nbValue = req.body.number;
    for (let index = 0; index < arrayOfNumbers.length; index++) {
      arrayOfNumbers[index] = arrayOfNumbers[index] + nbValue;
		}  
		saveList(arrayOfNumbers);  
    res.json({
      success: true,
      result: arrayOfNumbers
    })    
  }) 

router.route('/numberSoustraction')
  .post(function (req, res) {
    const nbValue = req.body.number;    
    for (let index = 0; index < arrayOfNumbers.length; index++) {
      arrayOfNumbers[index] = arrayOfNumbers[index] - nbValue;
		}    
		saveList(arrayOfNumbers);     
    res.json({
      success: true,
      result: arrayOfNumbers
    });    
  }) 

router.route('/numberMultiplication')
  .post(function (req, res) {
    const nbValue= req.body.number;    
    for (let index = 0; index < arrayOfNumbers.length; index++) {
      arrayOfNumbers[index] = arrayOfNumbers[index] * nbValue;
		}    
		saveList(arrayOfNumbers);   
    res.json({
      success: true,
      result: arrayOfNumbers
    });    
  }) 

router.route('/numberDivision')
  .post(function (req, res) {
		const nbValue= req.body.number;
		let success = true
		if (nbValue == 0){
			success= false
		}
		else{
			for (let index = 0; index < arrayOfNumbers.length; index++) {
				arrayOfNumbers[index] = arrayOfNumbers[index] / nbValue;
			}    
		}
		saveList(arrayOfNumbers);    
    res.json({
      success: success,
      result: arrayOfNumbers
    })    
  })

server.use(router);  

server.listen(port, hostname, function(){
	console.log(`Server running at: http://${hostname}:${port}`); 
});